# SSH Tunneling

## Getting start

The file [envVariables](envVariables) contains all the variable to use in the [Makefile](Makefile).

Example:
```
port=22
host=ingeint.com
user=ingeint
bindHost=localhost
bindPorts=8080:8080
```

- `port` remote ssh port.
- `host` remote ssh hostname or ip.
- `user` remote linux user.
- `bindPorts` remote internal hostname.
- `bindPorts` ports to bind, format  `"LOCAL:REMOTO"`, separated with space.

You could override the variables if it's necessary passing them as arguments.

## SSH connection examples

```
make
make ssh
make user=ingeint port=2222 host=ingeint.com
```

## Create tunnel examples
```
make tunnel
make tunnel bindHost=ingeint
make tunnel bindPorts="8080:8080 8081:8081"
make tunnel user=ingeint port=2222 host=ingeint.com bindPorts="8080:8080"
make tunnel user=ingeint host=ingeint.com bindPorts="8080:8080 8081:8081"
```
