-include envVariables
export

ssh:
	ssh -p $(port) $(user)@$(host)

tunnel:
	ssh -p $(port) $(user)@$(host) -T \
	$(foreach port,$(bindPorts),-L $(word 1,$(subst :, ,$(port))):$(bindHost):$(word 2,$(subst :, ,$(port))))
